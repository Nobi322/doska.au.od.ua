function initPhotoUpload(){

    var fileDiv = document.getElementById("upload");
    var fileInput = document.getElementById("upload-image");
    console.log(fileInput);
    fileInput.addEventListener("change",function(e){
        var files = this.files;
        showThumbnail(files);
    },false);

    fileDiv.addEventListener("click",function(e){
        $(fileInput).show().focus().click().hide();
        e.preventDefault();
    },false);

    fileDiv.addEventListener("dragenter",function(e){
        e.stopPropagation();
        e.preventDefault();
    },false);

    fileDiv.addEventListener("dragover",function(e){
        e.stopPropagation();
        e.preventDefault();
    },false);

    fileDiv.addEventListener("drop",function(e){
        e.stopPropagation();
        e.preventDefault();

        var dt = e.dataTransfer;
        var files = dt.files;

        showThumbnail(files);
    },false);

    $('.image-add').click(function (e) {
        e.preventDefault();
        $('#upload').click();
    });
    $(document).on('click', '.delete-img', function (e) {
        e.preventDefault();
        var $thimb = $(this).parents('.img-thumb');
        console.log('Image removed from list: '+$thimb.data('imgid'));
        $thimb.remove();
    });

    function showThumbnail(files){
        //$('.img-thumb').remove();
        for(var i=0;i<files.length;i++){
            var file = files[i]
            var imageType = /image.*/;
            if(!file.type.match(imageType)){
                console.log("Not an Image");
                continue;
            }

            var image = document.createElement("img");
            // image.classList.add("")
            var thumbnail = document.getElementById("thumbnail");
            image.file = file;
            $('.img-thumb.placeholder').first().before(image);
            //thumbnail.appendChild(image);
            $('#thumbnail img').last().prop('imgId',i);


            var reader = new FileReader();
            reader.onload = (function(aImg){
                return function(e){
                    aImg.src = e.target.result;
                };
            }(image))
            var ret = reader.readAsDataURL(file);
            var canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            image.onload= function(){
                ctx.drawImage(image,100,100)
            }
        }
        $('#thumbnail img:not(.max-photo)').each(function(){
            var $img = $(this);

            if(!$img.hasClass('wraped')){
                $img.wrap('<div data-imgId="'+$img.prop('imgId')+'" class="img-thumb"> </div>');

                $img.addClass('wraped');
                $img.addClass('masked');
                $img.after('<a href="#" class="delete-img fa-spin"></a>');
            }
        });
        //styleInputs();
        //$('.img-thumb img').wrap('<div class="img-cnt"></div>');

        $('.img-thumb img').load(function(){
            $(this).parents('.img-thumb').find('.fa-spin').removeClass('fa-spin');
            $(this).parents('.img-thumb').imagefill({throttle:200/60});
            /*setTimeout(function(){
             $('.img-cnt').imagefill({throttle:1000/60});
             },500);*/

        });
        //imgToBAckground('.img-thumb img');
    }
}
