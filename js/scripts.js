$(document).ready(function () {

    var wh = $(window).height()-$('footer').outerHeight()-150;

    $('.wrapper').css({
        'min-height': wh+'px'
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.expander-control').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        console.log('exp co');
        var $this = $(this).parents('.expander, .expander-multilevel').first();

        console.log($this);
        if(!$this.hasClass('open')){
            $this.addClass('open');

        }else{
            $this.removeClass('open');
        }
        $(this).blur();
    });

    $('.ipc-images').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Загрузка изображения #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">Данное изображение #%curr%</a> не может быть загружено.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        }
    });
    $(document).on('click', '.add-row', function (e) {
        e.preventDefault();
        var $parent = $(this).parents('.extendable-row');
        if($parent.find('input').val()!=''){
            $parent.find('input').removeClass('error');
            var $newr = $parent.clone(true);
            $newr.find('input').val('');
            $parent.after($newr);
            $parent.find('.add-phone-text').remove();
        }else{
            $parent.find('input').addClass('error');
        }
        $(this).blur();
    });
    $(document).on('change', '.pf-selector input[name="pf-change-check"]', function (e) {
       console.log('change');
       var $checked = $('.pf-selector input[name="pf-change-check"]:checked');
        console.log($checked.val());
        $('.pf-check-cnt').hide(0, function () {
            $('#'+$checked.val()).show();
        });

    });
    $(document).on('click', '.jq-popup', function (e) {
        e.preventDefault();
        $.magnificPopup.open({
            items: {
                src: $(this).attr('href'),
                type: 'inline'
            }
        });
    });
    $(document).on('click', function (e) {
        if($(e.target).parents('.jqs-select').length==0 && $('.jqs-select').hasClass('open')){
            $('.jqs-select').removeClass('open');

        }
        if($(e.target).parents('.aj2-select').length==0 && $('.aj2-select').hasClass('open')){
            $('.aj2-select').removeClass('open');

        }
    });
    $('.btn').click(function () {
       $(this).blur();
    });

    $('#pp-recover-pass').validate({
        rules:{
            "login-email":{
                email: true,
                minlength: 6,
                required: true
            }
        },
        messages:{
            "login-email":{
                email: "Email введен некоректно, исправьте и повторите.",
                minlength: "Минимальная длинна Email - 6 символов. ",
                required: "Вы забыли ввести Ваш Email"
            }
        }

    });
    $('.mini-form').validate({
        rules:{
            "login-email":{
                email: true,
                minlength: 6,
                required: true
            },
            "login-password":{
                minlength: 6,
                required: true,
                maxlength: 16
            },
            "login-password-confirm":{
                minlength: 6,
                required: true,
                maxlength: 16,
                equalTo: "#reg-pass"
            }

        },
        messages:{
            "login-email":{
                email: "Email введен некоректно, исправьте и повторите.",
                minlength: "Минимальная длинна Email - 6 символов. ",
                required: "Вы забыли ввести Ваш Email"

            },
            "login-password":{
                minlength: "Минимальная длинна пароля - 6 символов. ",
                maxlength: "Максимальная длинна пароля - 16 символов. ",
                required: "Вы забыли ввести Ваш пароль"
            },
            "login-password-confirm":{
                minlength: "Минимальная длинна пароля - 6 символов. ",
                maxlength: "Максимальная длинна пароля - 16 символов. ",
                required: "Вы забыли ввести Ваш пароль",
                equalTo: "Пароли не совпадают"
            }
        }

    });
    $('#change-password').validate({
        rules:{
            "new-pass":{
                minlength: 6,
                required: true,
                maxlength: 16
            },
            "new-pass-confirm":{
                minlength: 6,
                required: true,
                maxlength: 16,
                equalTo: ".confirm-pass"
            }

        },
        messages:{
            "new-pass":{
                minlength: "Минимальная длинна пароля - 6 символов. ",
                maxlength: "Максимальная длинна пароля - 16 символов. ",
                required: "Вы забыли ввести Ваш пароль"
            },
            "new-pass-confirm":{
                minlength: "Минимальная длинна пароля - 6 символов. ",
                maxlength: "Максимальная длинна пароля - 16 символов. ",
                required: "Вы забыли ввести Ваш пароль",
                equalTo: "Пароли не совпадают"
            }
        }

    });

    //Init block
    if($('.header-menu').length){
        initSearchLine();
    }
    styledPlaceholder();
    $('input:not(.dnt-style), select:not(.dnt-style)').styler({
        filePlaceholder: "<span>Прикрепить файлы</span>"
    });


    $('.jq-selectbox__dropdown ul').addClass('jq-select-list').mCustomScrollbar({
        advanced: {
            updateOnContentResize: true
        }
    });
    if($('.image-upload').length){
        initPhotoUpload();
    }

    initAjaxSelect();
    $('.item-image').imagefill();






   /*End ready*/
});

//$(document).on('')

$(document).on('click','.goto',function(e) {

    $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top-50
    }, 2000);
});

function initSearchLine(){
    //jq-search
    var $jqs = $('.jq-search'),
        $jqsInp = $jqs.find('.jqs-text-input'),
        $placehold = $jqs.find('.inp-placeholder'),
        $jSelect = $jqs.find('.jqs-select'),
        $sFirst = $jSelect.find('.jqs-l-first'),
        $sSecond = $jSelect.find('.jqs-l-second');

    console.log($jqsInp);

    function moreTop(){
        var top = $('.ui-autocomplete').offset().top+15;
        console.log(top);
        $('.ui-autocomplete').css('top',top+'px');
    }

    $jqsInp.autocomplete({
        delay: 500,
        minLength: 3,
        source: function(request, response) {
            $.ajax({
                type: "POST",
                url: "/searchRes.json",
                data: '{"Query":"'+request.term+'"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var array = data.error ? [] : $.map(data.search, function(m) {
                        return {
                            value: m.variant,
                            matched: m.matched
                        };
                    });
                    console.log(array);
                    response(array);
                },
                error: function (msg) {
                    alert(msg.status + ' ' + msg.statusText);
                }
            })
        },
        open: function( event, ui ) {
            moreTop();
        },
        change: function( event, ui ) {
            moreTop();
        }
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        return $("<li></li>").append('<span class="i-text">'+item.value+'</span><span class="i-qty">('+item.matched+' объяв.)</span>').appendTo(ul);
    };


    getDepartments();
    $(document).on('change', 'input[name="jqsl-1"]', function () {
        var dPrt = $('#jqs-l-first').find('input:checked').val();
        if(dPrt!=''||dPrt!=null||dPrt!=undefined){
            $('#jqs-select-input').val('');
            $('.jqs-return').text(dPrt);
            getCities(dPrt);
            showCty();
        }
    });
    $(document).on('change', 'input[name="jqsl-2"]', function () {
        var cTy = $('#jqs-l-second').find('input:checked').val();
        if(cTy!=''||cTy!=null||cTy!=undefined){
            $('#jqs-select-input').val(cTy);
            $('.jqs-select').removeClass('open');

        }
    });

    $(document).on('click', '.jqs-select-input, .jqs-s-caret', function (e) {

        e.preventDefault();
       var $jSel =  $(this).parents('.jqs-select'),
           $jSList = $jSel.find('.jqs-select-list');
        if(!$jSel.hasClass('open')){
            $jSel.addClass('open');
        }else{
            $jSel.removeClass('open');
        }
    });
    $(document).on('click', '.jqs-return', function (e) {
        e.preventDefault();
        showDpt();
    });

}

function showDpt(){
    $('#jqs-l-first').addClass('jq-l-visible');
    $('#jqs-l-second').removeClass('jq-l-visible');
}
function showCty(){
    $('#jqs-l-first').removeClass('jq-l-visible');
    $('#jqs-l-second').addClass('jq-l-visible');
}

function addDepartments(data){
    $('#jqs-l-first').html('');
    //console.log(data);
    data.forEach(function(item, i, arr) {
        var html = ' <label>';
        html +='<input type="radio" name="jqsl-1" value="'+item.department+'">';
        html +='<span>'+item.department+'</span>';
        html +='</label>';
        $('#jqs-l-first').append(html);
    });
}
function getDepartments(){
    var dpt = '';
    $('#jqs-l-first').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $.ajax({
        type: "POST",
        url: "/departments.json",
        data: '{"Query":"departments"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            dpt = data.error ? [] : $.map(data.departments, function(m) {
                return {
                    department: m.department
                };
            });
            //console.log(dpt.length);
            //console.log(dpt);
            addDepartments(dpt);
            showDpt();
        },
        error: function (msg) {
            $('#jqs-l-first').html('<h4>К сожелению ничего не найдено, попрбуйте позже ;)</h4>');
        }
    });


}

function addCities(data){
    $('#jqs-l-second').html('');
    //console.log(data);
    data.forEach(function(item, i, arr) {
        var html = '<label>';
        html +='<input type="radio" name="jqsl-2" value="'+item.city+'" data-departament="'+item.department+'">';
        html +='<span>'+item.city+'</span>';
        html +='</label>';
        $('#jqs-l-second').append(html);
    });
}
function getCities(departament){
    var cty = '';
    $('#jqs-l-second').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $.ajax({
        type: "POST",
        url: "/cityes.json",
        data: '{"Department":"'+departament+'"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            cty = data.error ? [] : $.map(data.cities, function(m) {
                return {
                    department: m.department,
                    city: m.city
                };
            });
            addCities(cty);
        },
        error: function (msg) {
            $('#jqs-l-second').html('<h4>К сожелению ничего не найдено, попрбуйте позже ;)</h4>');
        }
    });



}

function styledPlaceholder(){

    $('input.jq-placeholder:not(.jq-plsd)').each(function(){
        var $Inp = $(this).addClass('jq-plsd'),
            $plsh = $Inp.siblings('.inp-placeholder');

        var isContentEmpty = function(){
            var content = $Inp.val();
            return (content.length === 0) || false;
        };
        var showPlaceholder = function(){
            $Inp.addClass('placeholder');
        };
        var hidePlaceholder = function(){
            $Inp.removeClass('placeholder');
        };
        var inputFocused = function(){
            if(isContentEmpty()){
                hidePlaceholder(this);
            }
        };
        var inputBlurred = function(){
            if(isContentEmpty()){
                showPlaceholder(this);
            }
        };
        var parentFormSubmitted = function(){
            if(isContentEmpty()){
                hidePlaceholder(this);
            }
        };
        $Inp.focus(inputFocused);
        $Inp.blur(inputBlurred);
        $Inp.bind("parentformsubmitted", parentFormSubmitted);
        $plsh.click(function(){
            $Inp.focus();
        });
        if(isContentEmpty()){
            showPlaceholder();
        }
    });
}

function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}


function initAjaxSelect(){
    $('.ajax-2-select:not(.aj2-inited)').each(function (e) {
        //jq-search
        $(this).addClass('aj2-inited');

        if($(this).attr('id')==undefined || $(this).attr('id').length){
            $(this).attr('id',makeid());
        }
        var $ajqs = $(this),
            $ajSelect = $ajqs.find('.aj2-select'),
            $sFirst = $ajSelect.find('.aj2-l-first'),
            $sSecond = $ajSelect.find('.aj2-l-second');


        $sFirst
            .wrap('<div class="list-wrapper"></div>')
            .parent()
            .mCustomScrollbar({
            advanced:{
                updateOnContentResize: true
            }
        });
        $sSecond
            .wrap('<div class="list-wrapper hidden"></div>')
            .parent()
            .mCustomScrollbar({
            advanced:{
                updateOnContentResize: true
            }
        });


        getAjFirst($ajqs.attr('id'));

    });
    if(!$('body').hasClass('aj2-watch')){
        $('body').addClass('aj2-watch');
        $(document).on('change', 'input[name="aj2sl-1"]', function () {
            //Select first
            var $thisAJS = $(this).parents('.ajax-2-select'),
                firstSelected = $thisAJS.find('.aj2-l-first input:checked').val(),
                $ajinp = $thisAJS.find('.aj2-select-input');


            if(firstSelected!=''||firstSelected!=null||firstSelected!=undefined){
                $ajinp.val('');
                //$('.jqs-return').text(dPrt);
                getAjSecond( $thisAJS.attr('id'),firstSelected);
                $thisAJS.find('.aj2-l-second').parents('.list-wrapper').removeClass('hidden');
                // showCty();
            }
        });
        $(document).on('change', 'input[name="aj2sl-2"]', function () {
            //Select second
            var $thisAJS = $(this).parents('.ajax-2-select'),
                secondSelected = $thisAJS.find('.aj2-l-second input:checked').val(),
                $ajinp = $thisAJS.find('.aj2-select-input');


            if(secondSelected!=''||secondSelected!=null||secondSelected!=undefined){
                $ajinp.val(secondSelected);
                $thisAJS.find('.aj2-select').removeClass('open');

            }
        });

        $(document).on('click', '.aj2-select-input, .aj2-s-caret', function (e) {

            e.preventDefault();
            var $jSel =  $(this).parents('.aj2-select'),
                $jSList = $jSel.find('.aj2-select-list');
            if(!$jSel.hasClass('open')){
                $jSel.addClass('open');
            }else{
                $jSel.removeClass('open');
            }
        });
        $(document).on('click', '.aj2-return', function (e) {
            e.preventDefault();
            //showDpt();
            console.log('add action here');
        });
    }

}



function getAjFirst(parentId){

    var dpt = '',
        $parent = $('#'+parentId),
        $fl = $parent.find('.aj2-l-first'),
        $sl = $parent.find('.aj2-l-second');

    $fl.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $.ajax({
        type: "POST",
        url: $fl.data('url'),
        data: '{"Query":"firstlist"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            dpt = data.error ? [] : $.map(data.categories, function(m) {
                return {
                    category: m.category
                };
            });
            //console.log(dpt.length);
            console.log(dpt);
            addAjFirst(dpt,parentId);

        },
        error: function (msg) {
            $fl.html('<h4>К сожелению ничего не найдено, попрбуйте позже ;)</h4>');
        }
    });
}

function addAjFirst(data, parentId){

    var $parent = $('#'+parentId),
        $fl = $parent.find('.aj2-l-first'),
        $sl = $parent.find('.aj2-l-second');
    $fl.html('');
    //console.log(data);
    data.forEach(function(item, i, arr) {
        var html = ' <label>';
        html +='<input type="radio" name="aj2sl-1" value="'+item.category+'">';
        html +='<span>'+item.category+'</span>';
        html +='</label>';
        $fl.append(html);
    });
}


function getAjSecond(parentId, firtsData){

    //Var list
    var $parent = $('#'+parentId),
        $fl = $parent.find('.aj2-l-first'),
        $sl = $parent.find('.aj2-l-second');
    var dataSecond = '';
    $sl.html('<i class="fa fa-circle-o-notch fa-spin"></i>');
    $.ajax({
        type: "POST",
        url: $sl.data('url'),
        data: '{"ajax-list":"'+firtsData+'"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            dataSecond = data.error ? [] : $.map(data.subCategories, function(m) {
                return {
                    subCategory: m.subCategory
                };
            });
            addAjSecond(dataSecond, parentId);
        },
        error: function (msg) {
            $sl.html('<h4>К сожелению ничего не найдено, попрбуйте позже ;)</h4>');
        }
    });
}

function addAjSecond(data,parentId){
    //Var list
    var $parent = $('#'+parentId),
        $fl = $parent.find('.aj2-l-first'),
        $sl = $parent.find('.aj2-l-second');

    $sl.html('');
    //console.log(data);
    data.forEach(function(item, i, arr) {
        var html = '<label>';
        html +='<input type="radio" name="aj2sl-2" value="'+item.subCategory+'">';
        html +='<span>'+item.subCategory+'</span>';
        html +='</label>';
        $sl.append(html);
    });
}