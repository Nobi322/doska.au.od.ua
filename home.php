<?php include("header.php"); ?>

<div class="header-menu">
    <div class="container">
        <form class="row" name="search">
            <div class="col-md-10">
                <div class="jq-search ">
                    <input type="text" class="jqs-text-input jq-placeholder" name="search-line">
                    <div class="inp-placeholder">Что ищем? <span class="ip-m">(название товара)</span></div>
                    <div class="jqs-select">
                        <input class="jqs-select-input" id="jqs-select-input" disabled placeholder="Выберите город">
                        <a href="#" class="jqs-s-caret"><i class="fa fa-angle-right"></i> </a>
                        <div class="jqs-select-list">
                            <div id="jqs-l-first" class="jqs-l-first">

                            </div>
                            <div id="jqs-l-second" class="jqs-l-second">

                            </div>
                            <a href="#" class="jqs-return">

                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-default mt20">ПОИСК</button>
            </div>
        </form>
    </div>
</div>

    <div class="container pb50 pt40">
        <div class="row">
            <div class="col-md-12 text-center">
                <h3>Выберите категорию объявления:</h3>
            </div>

        </div>

        <div class="row">

            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-nedv.png">
                </div>
                <p>
                    Недвижимость
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-elect.png">
                </div>
                <p>
                    Электроника
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-hobby.png">
                </div>
                <p>
                    Хобби и отдых
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-trans.png">
                </div>
                <p>
                    Транспорт
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-work.png">
                </div>
                <p>
                    Работа
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-anim.png">
                </div>
                <p>
                    Животные
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-child.png">
                </div>
                <p>
                    Детский мир
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-biz.png">
                </div>
                <p>
                    Бизнес и услуги
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-mod.png">
                </div>
                <p>
                    Мода и стиль
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-home.png">
                </div>
                <p>
                    Дом и сад
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-free.png">
                </div>
                <p>
                    Отдам даром
                </p>
            </a>
            <a href="#" class="col-md-2 main-cat">
                <div class="img-cnt">
                    <img src="img/mc-change.png">
                </div>
                <p>
                    Обмен
                </p>
            </a>
        </div>
        <div class="row">
            <div class="col-md-12 mp-plaha">
                <h1>О сервисе “Доска объявлений”</h1>
                <p>
                    Бесплатные объявления Украины на OLX.ua - здесь вы найдете то, что искали! Нажав на  		кнопку "Подать объявление", вы перейдете на форму, заполнив которую, сможете 				разместить объявление на любую необходимую тематику легко и абсолютно бесплатно.
                </p>
            </div>
        </div>
    </div>







<?php include("footer.php"); ?>