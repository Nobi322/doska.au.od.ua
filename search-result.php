<?php include("header.php"); ?>

<div class="header-menu">
    <div class="container">
        <form class="row" name="search">
            <div class="col-md-9">
                <div class="jq-search ">
                    <input type="text" class="jqs-text-input jq-placeholder" name="search-line">
                    <div class="inp-placeholder">Что ищем? <span class="ip-m">(название товара)</span></div>
                    <div class="jqs-select">
                        <input class="jqs-select-input" id="jqs-select-input" disabled placeholder="Выберите город">
                        <a href="#" class="jqs-s-caret"><i class="fa fa-angle-right"></i> </a>
                        <div class="jqs-select-list">
                            <div id="jqs-l-first" class="jqs-l-first">

                            </div>
                            <div id="jqs-l-second" class="jqs-l-second">

                            </div>
                            <a href="#" class="jqs-return">

                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="ajax-2-select">
                    <div class="aj2-select">
                        <input class="aj2-select-input" id="" disabled placeholder="Выберите категорию">
                        <a href="#" class="aj2-s-caret"><i class="fa fa-angle-right"></i> </a>
                        <div class="aj2-select-list">
                            <div class="aj2-l-second" data-url="/sub-cat-list.json">

                            </div>
                            <div class="aj2-l-first" data-url="/cat-list.json">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row pt20 pb25">
            <div class="col-md-3">
                <select class="select-1">
                    <option selected>От дешевых к дорогим</option>
                    <option>От дорогих к дешевым</option>
                </select>
            </div>
            <div class="col-md-3">
                <select class="select-1">
                    <option selected>От новых к старым</option>
                    <option>От старых к новым</option>
                </select>
            </div>
            <div class="col-md-3">

            </div>
            <div class="col-md-2 col-md-offset-1">
                <button type="submit" class="btn btn-search">ПОИСК <i class="fa fa-search"></i> </button>
            </div>
        </div>
    </div>
</div>

    <div class="container pb50 pt40">
        <div class="row">

            <?php
            $i =0;
            $title = array(
                'Продам настольный пк ',
                'Продам Монитор и Системный блок! Lighting ХX-027',
                'GeForce GTX 570 Asus DirectCU II ',
                'Жёсткий диск 500Gb 2.5" 7200 rpm  Toshiba HDD Кеш 16 Мб'
                );

            while($i++<7):
            ?>
            <div class="col-md-12 item-pill <?php echo $i%2>0 ? 'item-top': '';  echo $i%2==0 ? ' item-torg': ''; ?>">
                <div class="row ip-cnt">
                    <a href="#" class="col-md-3 image-cnt">
                        <img src="img/ip-<?php echo rand(1,4) ?>.png">
                    </a>
                    <div class="col-md-9 ip-info">
                        <div class="row row-1">
                            <div class="col-md-7 ip-text">
                                <a href="#" class="ip-title">
                                    <?php echo $title[rand(0,3)]; ?>
                                </a>
                                <span class="ip-top">(ТОП)</span>
                                <p class="ip-date">
                                    09.03.2015 в 17:20
                                </p>
                            </div>
                            <div class="col-md-4 col-md-offset-1">
                                <a href="#" class="ip-price">
                                    24 760 <span class="currency">грн.</span>
                                </a>
                                <div class="ip-message">
                                    <i class="i-msg"></i>
                                    Торги
                                </div>
                            </div>
                        </div>
                        <div class="row row-2">
                            <div class="col-md-7">
                                <p class="ip-cat">
                                    Электроника / Настольные ПК / Киев
                                </p>
                            </div>
                            <div class="col-md-4 col-md-offset-1">
                                <a href="#" class="ip-favorite">
                                    <i class="i-star"></i>
                                    <span>В избранное</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>




        </div>
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <ul class="pagination">
                        <li class="prev">
                            <a href="#" aria-label="Previous">
                                <i class="i-arrow-left"></i>
                                <span>Назад</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li class="disabled"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="next">
                            <a href="#" aria-label="Next">
                               <span>Следующая</span>
                                <i class="i-arrow-right"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>


    </div>







<?php include("footer.php"); ?>