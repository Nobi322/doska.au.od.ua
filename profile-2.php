<?php include("header.php"); ?>



    <div class="container pb50 pt40">
        <div class="row mb25">
            <div class="col-md-12">
                <div class="profile-pagi">
                   <ul class="pagi-list">
                       <li>
                           <a href="#">Профиль</a>
                       </li>
                       <li class="active">
                           <a href="#">Объявления</a>
                       </li>
                       <li>
                           <a href="#">Избранное</a>
                       </li>
                   </ul>
                </div>
            </div>

        </div>
        <div class="row">

            <?php
            $i =0;
            $title = array(
                'Продам настольный пк ',
                'Продам Монитор и Системный блок! Lighting ХX-027',
                'GeForce GTX 570 Asus DirectCU II ',
                'Жёсткий диск 500Gb 2.5" 7200 rpm  Toshiba HDD Кеш 16 Мб'
            );

            while($i++<4):
                ?>
                <div class="col-md-12 item-pill lk-advert">
                    <div class="row ip-cnt">
                        <div class="col-md-12 ip-info">
                            <div class="row row-1">
                                <div class="col-md-8 ip-text">
                                    <a href="#" class="ip-title">
                                        <?php echo $title[rand(0,3)]; ?>
                                    </a>
                                    <span class="ip-top">(ТОП)</span>
                                    <p class="ip-date">
                                        09.03.2015 в 17:20
                                    </p>
                                </div>
                                <div class="col-md-3 col-md-offset-1">
                                    <a href="#" class="btn btn-info mt5">
                                        <?php echo $i%2>0 ? 'продлить': 'поднять в топ' ?>
                                    </a>
                                </div>
                            </div>
                            <div class="row row-btn">
                                <div class="col-md-3">
                                    <a href="#" class="link-iconed l-show">
                                        <i class="icon"></i>
                                        Просмотреть
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#" class="link-iconed l-edit">
                                        <i class="icon"></i>
                                        Редактировать
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#" class="link-iconed l-activate">
                                        <i class="icon"></i>
                                        Активировать
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#" class="link-iconed  l-delete">
                                        <i class="icon"></i>
                                        Удалить
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>




        </div>



    </div>







<?php include("footer.php"); ?>