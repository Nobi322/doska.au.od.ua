<?php include("header.php"); ?>

<div class="container pt50 pb50">
    <div class="row">
        <div class="col-md-12">
            <h1>Страницы</h1>
            <ol>
                <li>
                    <a href="home.php" target="_blank">Главная</a>
                </li>
                <li>
                    <a href="home-subcat.php" target="_blank">Главная - подкатегории</a>
                </li>
                <li>
                    <a href="search-result.php" target="_blank">Поисковая выдача</a>
                </li>
                <li>
                    <a href="item-page.php" target="_blank">Странница товара</a>
                </li>
                <li>
                    <a href="all-advertisement.php" target="_blank">Все объявления автора</a>
                </li>
                <li>
                    <a href="login.php" target="_blank">Вход</a>
                </li>
                <li>
                    <a href="register.php" target="_blank">Регистрация</a>
                </li>
                <li>
                    <a href="profile-1.php" target="_blank">Личный кабинет - Профиль</a>
                </li>
                <li>
                    <a href="profile-2.php" target="_blank">Личный кабинет - Объявления</a>
                </li>
                <li>
                    <a href="profile-3.php" target="_blank">Личный кабинет - Избранное</a>
                </li>
                <li>
                    <a href="new-advertisment.php" target="_blank">Создание нового объявления</a>
                </li>
                <li>
                    <a href="go-top.php" target="_blank">Поднять в топ</a>
                </li>
                <li class="hidden">
                    <a href="payment.php" target="_blank">Способы оплаты</a>
                </li>
                <li>
                    <a href="FAQ.php" target="_blank">FAQ</a>
                </li>
                <li>
                    <a href="contact-form.php" target="_blank">Контактная форма</a>
                </li>
                <li>
                    <a href="rules.php" target="_blank">Правила пользования</a>
                </li>
            </ol>
            <h1>Всплывашки</h1>
            <ol>
                <li>
                    <a href="#pp-reg-thx" class="jq-popup">Благодарность за регистрацию</a>
                </li>
                <li>
                    <a href="#pp-msg-thx" class="jq-popup">Благодарность за сообщение</a>
                </li>
                <li>
                    <a href="#pp-delete-adv" class="jq-popup">Вы действительно  хотите удалить объявление?</a>
                </li>
                <li>
                    <a href="#pp-thx-adv" class="jq-popup">Ваше объявление будет опубликовано,  после прохождения модераци</a>
                </li>
                <li>
                    <a href="#pp-thx-msg" class="jq-popup">Благодарим за Ваш вопрос.</a>
                </li>
                <li>
                    <a href="#pp-recover-pass" class="jq-popup">Восстановление пароля</a>
                </li>
                <li>
                    <a href="#pp-recover-send" class="jq-popup">На Ваш email был отправлен  временный пароль.</a>
                </li>
            </ol>
        </div>
    </div>
</div>




<div id="pp-reg-thx" class="mfp-hide white-popup text-center">
    <h3>Благодарим за регистрацию</h3>
    <p>
        На ваш почтовый ящик <b>было отправлено
            письмо</b> для подтверждения email адреса.
        Для завершения регистрации перейдите
        по ссылке в письме.
    </p>
</div>
<div id="pp-msg-thx" class="mfp-hide white-popup text-center">
    <h3>Благодарим за Ваше сообщение.</h3>
    <p>
        <b>Мы свяжемся с Вами</b> в ближайшее
        время и ответим на него.
    </p>
</div>
<div id="pp-delete-adv" class="mfp-hide white-popup text-center">
    <h3>Вы действительно  хотите удалить объявление?</h3>
    <div CLASS="row mt20">
        <div class="col-md-4 col-md-offset-2">
            <a href="#" class="btn btn-danger">да</a>
        </div>
        <div class="col-md-4">
            <a href="#" class="btn btn-success">нет</a>
        </div>
    </div>
</div>

<div id="pp-thx-adv" class="mfp-hide white-popup text-center">
    <h3>Спасибо!</h3>
    <br>
    <p>
         Ваше объявление <b>будет опубликовано</b>,  после прохождения модераци
    </p>
</div>

<div id="pp-thx-msg" class="mfp-hide white-popup text-center">
    <h3>Благодарим за Ваш вопрос.</h3>
    <br>
    <p>
        <b>Мы свяжемся с Вами</b> в ближайшее  время и ответим на него.
    </p>
</div>

<form id="pp-recover-pass" class="mfp-hide white-popup narrow text-center">
    <h4>Для востановления пароля
        <b>введите Ваш email</b></h4>
    <div class="row mt30">
        <div class="col-md-12">
            <div class="input-group">
                <input type="text" class="form-control" name="login-email" placeholder="Введите Ваш e-mail " required>
                <i class="fa fa-envelope-o input-group-addon"></i>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-info">восстановить пароль</button>
        </div>
    </div>
</form>


<div id="pp-recover-send" class="mfp-hide white-popup text-center">
    <h3>На Ваш email <b>был отправлен</b>  временный пароль.</h3>
    <br>
    <p>
        Измените его после первого входа.
    </p>
</div>


<?php include("footer.php"); ?>