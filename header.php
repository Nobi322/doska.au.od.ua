<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1020">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Доска объявлений</title>

    <!-- Plugins -->
    <link href="css/jquery.formstyler.css" rel="stylesheet">
    <link href="css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/jquery-ui.min.css" rel="stylesheet">

    <!-- Main style -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <a href="#" class="col-md-2 logo">
                <img src="img/logo-2.png">
            </a>
            <div class="col-md-6 col-md-offset-1">
                <a href="#" class="btn btn-create center-block">
                    Создать объявление
                </a>
            </div>
            <div class="col-md-3 text-right r-col">
                <a href="#" class="reg-link">
                    Регистрация
                </a>
                <a href="#" class="btn btn-dashed login-link">
                    Войти
                </a>
            </div>
        </div>
    </div>
</header>
<div class="wrapper">

