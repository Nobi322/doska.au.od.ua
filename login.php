<?php include("header.php"); ?>



    <div class="container pb50 pt80">
        <div class="row">
            <div class="col-md-4-5 center-block">
                <form class="mini-form">
                    <h3>Вход в личный кабинет</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="email" class="form-control" name="login-email" placeholder="Введите Ваш e-mail " required>
                                <i class="fa fa-envelope-o input-group-addon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row mb40">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="password" class="form-control" name="login-password" placeholder="Введите Ваш пароль " required>
                                <i class="fa fa-lock input-group-addon"></i>
                            </div>
                            <a href="#" class="form-link">Забыли пароль?</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-iconed-left lg">
                                <i class="fa fa-user"></i>
                                войти
                            </button>
                        </div>
                    </div>
                    <div class="row mt20">
                        <div class="col-md-12">
                            <label>Еще не регистрировались? Это не долго!</label>
                            <a href="#" class="btn btn-info">зарегистрироваться</a>
                        </div>
                    </div>

                </form>
            </div>
        </div>


    </div>







<?php include("footer.php"); ?>