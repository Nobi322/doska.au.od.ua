<?php include("header.php"); ?>



    <div class="container pb50 pt40">
        <div class="row mb25">
            <div class="col-md-12">
                <div class="profile-pagi">
                   <ul class="pagi-list">
                       <li>
                           <a href="#">Профиль</a>
                       </li>
                       <li>
                           <a href="#">Объявления</a>
                       </li>
                       <li class="active">
                           <a href="#">Избранное</a>
                       </li>
                   </ul>
                </div>
            </div>

        </div>
        <div class="row">

            <?php
            $i =0;
            $title = array(
                'Продам настольный пк ',
                'Продам Монитор и Системный блок! Lighting ХX-027',
                'GeForce GTX 570 Asus DirectCU II ',
                'Жёсткий диск 500Gb 2.5" 7200 rpm  Toshiba HDD Кеш 16 Мб'
            );

            while($i++<4):
                ?>
                <div class="col-md-12 item-pill">
                    <div class="row ip-cnt">
                        <a href="#" class="col-md-3 image-cnt">
                            <img src="img/ip-<?php echo rand(1,4) ?>.png">
                        </a>
                        <div class="col-md-9 ip-info">
                            <div class="row row-1">
                                <div class="col-md-7 ip-text">
                                    <a href="#" class="ip-title">
                                        <?php echo $title[rand(0,3)]; ?>
                                    </a>
                                    <span class="ip-top">(ТОП)</span>
                                    <p class="ip-date">
                                        09.03.2015 в 17:20
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <a href="#" class="ip-price">
                                        24 760 <span class="currency">грн.</span>
                                    </a>
                                    <div class="ip-message">
                                        <i class="i-msg"></i>
                                        Торги
                                    </div>
                                </div>
                            </div>
                            <div class="row row-2">
                                <div class="col-md-7">
                                    <p class="ip-cat">
                                        Электроника / Настольные ПК / Киев
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <a href="#" class="link-iconed  l-delete">
                                        <i class="icon"></i>
                                        Удалить из избранного
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>




        </div>




    </div>







<?php include("footer.php"); ?>