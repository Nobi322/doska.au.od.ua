<?php include("header.php"); ?>



    <div class="container pb50 pt80">
        <div class="row">
            <div class="col-md-4-5 center-block">
                <form class="mini-form">
                    <h3>Регистрация</h3>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="text" class="form-control" name="login-email" placeholder="Введите Ваш e-mail ">
                                <i class="fa fa-envelope-o input-group-addon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input id="reg-pass" type="password" class="form-control" name="login-password" placeholder="Введите Ваш пароль ">
                                <i class="fa fa-lock input-group-addon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="input-group">
                                <input type="password" class="form-control" name="login-password-confirm" placeholder="Повторите Ваш пароль ">
                                <i class="fa fa-lock input-group-addon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-info">
                                зарегистрироваться
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>


    </div>







<?php include("footer.php"); ?>