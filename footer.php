
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Рубрики на доске объявлений:</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="f-list">
                    <li>
                        <a href="#">Ответы на вопросы </a>
                    </li>
                    <li>
                        <a href="#">Правила пользования</a>
                    </li>
                    <li>
                        <a href="#">Обратная связь</a>
                    </li>
                    <li>
                        <a href="#">Подать объявление</a>
                    </li>
                    <li>
                        <a href="#"> Политика конфиденциальности</a>
                    </li>
                    <li>
                        <a href="#">  Личный кабинет</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row f-shadow">
            <a href="#" class="col-md-2">
                <img src="img/logo-1.png">
            </a>
            <div class="col-md-6 col-md-offset-1">
               <p class="f-text">
                   С помощью сайта объявлений Olx Украина вы сможете купить или продать из рук в руки практически все, что угодно.
               </p>
            </div>
            <div class="col-md-3  text-right r-col">
                <ul class="social s-footer">
                    <li>
                        <a href="#" class="fb" data-toggle="tooltip" data-placement="top" title="Facebook social"></a>
                    </li>
                    <li>
                        <a href="#" class="tw" data-toggle="tooltip" data-placement="top" title="Twitter social"></a>
                    </li>
                    <li>
                        <a href="#" class="ig" data-toggle="tooltip" data-placement="top" title="Instagram social"></a>
                    </li>
                    <li>
                        <a href="#" class="gp" data-toggle="tooltip" data-placement="top" title="Google+ social"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Min functional scripts -->
<script src="js/jquery-1.11.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!-- JavaScript Plugins -->
<script src="js/plugins.js"></script>
<script src="js/jquery.formstyler.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.mCustomScrollbar.min.js"></script>
<script src="js/image-upload.js"></script>

<!-- Main script-->
<script src="js/scripts.js"></script>

</body>
</html>