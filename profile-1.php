<?php include("header.php"); ?>




    <div class="container pb50 pt40">
        <div class="row mb25">
            <div class="col-md-12">
                <div class="profile-pagi">
                   <ul class="pagi-list">
                       <li class="active">
                           <a href="#">Профиль</a>
                       </li>
                       <li>
                           <a href="#">Объявления</a>
                       </li>
                       <li>
                           <a href="#">Избранное</a>
                       </li>
                   </ul>
                </div>
            </div>

        </div>
       <div class="row">
           <div class="col-md-12">
                <div class="profle-cnt">
                    <h1 class="text-center">Контактные данные :</h1>
                    <div class="row mt25">
                        <div class="col-md-6">
                            <label class="form-text">
                                Область
                            </label>
                            <select class="select-2" data-placeholder="Выберите Вашу область">
                                <option></option>
                                <option>Одесская</option>
                                <option>Киевская</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>Донецкая</option>
                                <option>и т.д.</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label class="form-text">
                                Город
                            </label>
                            <select class="select-2" data-placeholder="Выберите Ваш город  ">
                                <option></option>
                                <option>Одесса</option>
                                <option>Киев</option>
                                <option>Донецк</option>
                                <option>Львов</option>
                                <option>и т.д.</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-text">
                                Имя
                            </label>
                            <input type="text" class="form-control" placeholder="Введите Ваше имя ">
                        </div>
                    </div>
                    <div class="row extendable-row">
                        <div class="col-md-6">
                            <label class="form-text">
                                Номер телефона
                            </label>
                            <input type="text" class="form-control" placeholder="Введите Ваш номер телефона">
                        </div>
                        <div class="col-md-6">
                            <a href="#" class="add-row space-top">+</a>
                            <img src="img/add-phone-text.png" class="add-phone-text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-text">
                                Skype
                            </label>
                            <input type="text" class="form-control" placeholder="Введите Ваш Skype">
                        </div>
                    </div>
                    <div class="row mb20">
                        <div class="col-md-12">
                            <div class="profile-pagi">
                                <div class="row pf-selector">
                                    <div class="col-md-6 text-center">
                                        <label class="pfs-label">
                                            <input type="radio" name="pf-change-check" class="dnt-style" value="change-email">
                                            <span class="pfs-text">
                                                <span>
                                                    Изменить email
                                                </span>
                                                <i class="fa fa-angle-down"></i>
                                            </span>
                                        </label>
                                    </div>
                                    <div class="col-md-6 text-center">
                                        <label class="pfs-label">
                                            <input type="radio" name="pf-change-check" class="dnt-style" value="change-password">
                                            <span class="pfs-text">
                                                <span>
                                                     Изменить пароль
                                                </span>
                                                <i class="fa fa-angle-down"></i>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="change-email" name="change-email" class="row pf-check-cnt">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-text">
                                        Текущий e-mail
                                    </label>
                                    <input type="text" class="form-control" placeholder="Введите Ваш текущий e-mail ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-text">
                                        Новый e-mail
                                    </label>
                                    <input type="text" class="form-control" placeholder="Введите Ваш новый e-mail  ">
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="change-password" name="change-password" class="row pf-check-cnt">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-text">
                                        Текущий пароль
                                    </label>
                                    <input type="password" class="form-control" placeholder="Введите Ваш текущий пароль  ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-text">
                                        Новый пароль
                                    </label>
                                    <input type="password" name="new-pass" class="form-control confirm-pass" placeholder="Введите Ваш новый пароль   ">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="form-text">
                                        Повторите новый пароль
                                    </label>
                                    <input type="password" name="new-pass-confirm" class="form-control" placeholder="Повторите новый пароль">
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-md-5">
                            <button type="submit" class="btn btn-info">Сохранить изменения</button>
                        </div>
                    </div>
                </div>
           </div>
       </div>


    </div>







<?php include("footer.php"); ?>
<script>
    /* при програминге удалить */
    $('.reg-link').text('Пупкин');
    $('.login-link').text('Выход');
</script>
