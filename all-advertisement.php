<?php include("header.php"); ?>



    <div class="container pb50 pt20">
        <div class="row mb25">
            <div class="col-md-12">
                <h1>Все объявления автора Николая</h1>
                <div class="prev-bt">
                    <a href="#" aria-label="Previous">
                        <i class="i-arrow-left"></i>
                        <span>Назад</span>
                    </a>
                </div>
            </div>

        </div>
        <div class="row">

            <?php
            $i =0;
            $title = array(
                'Продам настольный пк ',
                'Продам Монитор и Системный блок! Lighting ХX-027',
                'GeForce GTX 570 Asus DirectCU II ',
                'Жёсткий диск 500Gb 2.5" 7200 rpm  Toshiba HDD Кеш 16 Мб'
            );

            while($i++<4):
                ?>
                <div class="col-md-12 item-pill">
                    <div class="row ip-cnt">
                        <a href="#" class="col-md-3 image-cnt">
                            <img src="img/ip-<?php echo rand(1,4) ?>.png">
                        </a>
                        <div class="col-md-9 ip-info">
                            <div class="row row-1">
                                <div class="col-md-7 ip-text">
                                    <a href="#" class="ip-title">
                                        <?php echo $title[rand(0,3)]; ?>
                                    </a>
                                    <span class="ip-top">(ТОП)</span>
                                    <p class="ip-date">
                                        09.03.2015 в 17:20
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <a href="#" class="ip-price">
                                        24 760 <span class="currency">грн.</span>
                                    </a>
                                    <div class="ip-message">
                                        <i class="i-msg"></i>
                                        Торги
                                    </div>
                                </div>
                            </div>
                            <div class="row row-2">
                                <div class="col-md-7">
                                    <p class="ip-cat">
                                        Электроника / Настольные ПК / Киев
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <a href="#" class="ip-favorite">
                                        <i class="i-star"></i>
                                        <span>В избранное</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>




        </div>


    </div>







<?php include("footer.php"); ?>