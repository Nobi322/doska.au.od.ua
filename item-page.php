<?php include("header.php"); ?>



    <div class="container pb50 pt40">
        <div class="row mb25">
            <div class="col-md-12">
                <div class="item-pagi">
                    <div class="prev">
                        <a href="#" aria-label="Previous">
                            <i class="i-arrow-left"></i>
                            <span>Назад</span>
                        </a>
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="#">Электроника</a></li>
                        <li><a href="#">Настольные ПК</a></li>
                        <li class="active">Киев</li>
                    </ol>
                    <div class="next">
                        <a href="#" aria-label="Next">
                            <span>Следующее объявление</span>
                            <i class="i-arrow-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="item-page-container">
                    <div class="row mb20">
                        <div class="col-md-10 ipc-title">
                            <h1>Продам MacBook Air 13.3 md761</h1>
                        </div>
                        <div class="col-md-2 ipc-sub">
                            Б/У, Торг
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <p class="ipc-date">
                                Опубликовано: 09.03.2015 в 17:20
                            </p>
                        </div>
                        <div class="col-md-5 text-right">
                            <a href="#" class="ip-favorite">
                                <i class="i-star"></i>
                                <span>В избранное</span>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ipc-images">
                            <?php
                            $i =0;

                            while($i++<5):
                                ?>
                            <a class="item-image" href="img/ipc-<?php echo $i ?>.png">
                                <img src="img/ipc-<?php echo $i ?>.png">
                            </a>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ipc-text">
                            <p>
                                Информация, размещенная на сайте об увеличении количества новых клиентов, ро- сте продаж, улучшении конверсий, и развитии бизнеса в целом, также примеры дос- тижения данных показателей сами по себе не гарантируют, что Вы получите такие же или лучшие результаты.

                            </p>

                            <ul>
                                <li>быстрый</li>
                                <li> удобный</li>
                                <li>качественный</li>
                            </ul>
                            <p>
                                Ваши результаты зависят только от Вас, от алгоритмов, которые Вы применяете в своей работе, Ваших этических принципов, деловых качеств и навыков. Мы не мо- жем нести ответственность за результаты работы Вашей компании.

                            </p>
                            <p>
                                Информация, размещенная на сайте об увеличении количества новых клиентов, ро- сте продаж, улучшении конверсий, и развитии бизнеса в целом, также примеры дос- тижения данных показателей сами по себе не гарантируют.

                            </p>
                        </div>
                    </div>

                </div>
                <div class="row mb25 mt20">
                    <div class="col-md-12">
                        <div class="item-pagi">
                            <div class="prev">
                                <a href="#" aria-label="Previous">
                                    <i class="i-arrow-left"></i>
                                    <span>Назад</span>
                                </a>
                            </div>
                            <div class="next">
                                <a href="#" aria-label="Next">
                                    <span>Следующее объявление</span>
                                    <i class="i-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb20">
                    <div class="col-md-12">
                        <h1>Свяжитесь с автором объявления </h1>
                    </div>
                    <div class="col-md-12 ">
                        <div class="cont-fon">
                            <p class="c-phone">
                                <span class="t-code">+38</span>
                                (066) 665 88 54
                            </p>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <form id="write-author" class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-text">
                                    E-mail
                                </label>
                                <input type="text" class="form-control" placeholder="Введите Ваш e-mail">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb10">
                                <label class="form-text">
                                    Сообщение
                                </label>
                                <textarea class="form-control" placeholder="Введите Ваше сообщение автору"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb30">
                                <input type="file" multiple>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-info">написать</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <aside class="sidebar col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ipc-price">
                            19 410 <span class="currency">грн.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ipc-contact">
                        <h4>Связаться с автором:</h4>
                        <div class="ipc-contact-cnt">
                            <h3>Николай</h3>
                            <a href="#" class="all-items">
                                Все объявления автора
                            </a>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="#write-author" class="btn btn-iconed-left goto">
                                        <i class="fa fa-envelope-o"></i>
                                        Написать автору
                                    </a>
                                </div>
                            </div>
                            <div class="row expander pt20">
                                <div class="col-md-12">
                                    <a href="#" class="expander-control only-hide">
                                        Показать контакты автора
                                        <i class="fa fa-angle-down exp-caret"></i>
                                    </a>
                                </div>
                                <div class="col-md-12 expander-container">
                                    <p class="c-phone">
                                        <span class="t-code">+38</span>
                                        (066) 665 88 54
                                    </p>
                                    <p class="c-phone">
                                        <span class="t-code">+38</span>
                                        (066) 665 88 54
                                    </p>
                                    <p class="c-other">
                                        <span class="c-title">
                                            skype:
                                        </span>
                                        loginskype
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>

        </div>


    </div>







<?php include("footer.php"); ?>