<?php include("header.php"); ?>



    <div class="container pb50 pt40">
        <div class="row">
           <div class="col-md-8 center-block">
               <h1 class="text-center">Создание бесплатного объявления: </h1>

               <div class="row mt25">
                   <div class="col-md-12">
                       <label class="form-text">
                           Название объявления:
                       </label>
                       <input type="text" class="form-control chalk-form" placeholder="Введите название Вашего объявления ">
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <label class="form-text">
                           Категория объявления
                       </label>
                       <select class="select-3" data-placeholder="Выберите категорию">
                           <option></option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                       </select>
                   </div>
                   <div class="col-md-6">
                       <label class="form-text">
                           Подкатегория объявления
                       </label>
                       <select class="select-3 disabled" data-placeholder="Выберите подкатегорию">
                           <option></option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                       </select>
                       <label class="form-text-mini">
                           (для началы выберите категорию)
                       </label>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-12">
                       <div class="profile-pagi">
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="chb">
                                        <input type="checkbox">
                                        Обмен
                                    </label>
                                </div>
                                <div class="col-md-3">
                                    <label class="chb">
                                        <input type="checkbox">
                                        Торг
                                    </label>
                                </div>
                            </div>
                       </div>
                   </div>
               </div>
               <div class="row mt25">
                   <div class="col-md-5">
                       <label class="form-text">
                           Цена:
                       </label>
                       <input type="text" class="form-control chalk-form" placeholder="Введите цену ">
                   </div>
                   <div class="col-md-5 pt35">
                       <label class="chb">
                           <input type="checkbox">
                           Договорная
                       </label>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <select class="select-3">
                           <option>Новое</option>
                           <option>БУ</option>
                       </select>
                   </div>
                   <div class="col-md-6 pt10">
                       <img src="img/new-bu--text.png">
                   </div>
               </div>

               <div class="row">
                   <div class="col-md-12">
                       <label class="form-text">
                           Описание объявления:
                       </label>
                       <div class="row chalk-text-cnt">
                           <textarea class="form-control" placeholder="Введите описание объявления"></textarea>
                       </div>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-5">
                       <label class="form-text">
                           Загрузите фотографии:
                       </label>
                   </div>
               </div>
               <div class="image-upload row mb40">

                   <div class="col-md-12">
                       <div id="upload" class="drop-area">

                       </div>
                       <div id="thumbnail">
                           <!-- image thumbnails field -->


                           <!-- Placeholders -->
                           <div class="img-thumb placeholder">
                               <a href="#" class="image-add"></a>
                           </div>
                           <div class="img-thumb placeholder">
                               <a href="#" class="image-add"></a>
                           </div>
                           <div class="img-thumb placeholder">
                               <a href="#" class="image-add"></a>
                           </div>
                           <div class="img-thumb placeholder">
                               <a href="#" class="image-add"></a>
                           </div>
                           <div class="img-thumb placeholder">
                               <a href="#" class="image-add"></a>
                           </div>
                           <div class="img-thumb placeholder">
                               <a href="#" class="image-add"></a>
                           </div>
                           <img src="img/icons/max-phote-text.png" class="max-photo">
                       </div>
                       <input type="file" style="display:none" id="upload-image" multiple="multiple" class="dnt-style">

                   </div>
               </div>
               <h1 class="text-center">Контактные данные:</h1>


               <div class="row mt25">
                   <div class="col-md-12">
                       <label class="form-text">
                           Контактное лицо:
                       </label>
                       <input type="text" class="form-control" placeholder="Введите контактное лицо">
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                       <label class="form-text">
                           Область
                       </label>
                       <select class="select-2" data-placeholder="Выберите Вашу область">
                           <option></option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                       </select>
                   </div>
                   <div class="col-md-6">
                       <label class="form-text">
                           Город
                       </label>
                       <select class="select-2 disabled" data-placeholder="Выберите Ваш город">
                           <option></option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                           <option>Электроника</option>
                           <option>Дом</option>
                           <option>Сад</option>
                           <option>Хобби</option>
                           <option>Электроника</option>
                       </select>
                       <label class="form-text-mini">
                           (для начала выберите область)
                       </label>
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-12">
                       <label class="form-text">
                           Имя
                       </label>
                       <input type="text" class="form-control" placeholder="Введите Ваше имя ">
                   </div>
               </div>
               <div class="row extendable-row">
                   <div class="col-md-6">
                       <label class="form-text">
                           Номер телефона
                       </label>
                       <input type="text" class="form-control" placeholder="Введите Ваш номер телефона">
                   </div>
                   <div class="col-md-6">
                       <a href="#" class="add-row space-top">+</a>
                       <img src="img/add-phone-text.png" class="add-phone-text">
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-12">
                       <label class="form-text">
                           Skype
                       </label>
                       <input type="text" class="form-control" placeholder="Введите Ваш Skype">
                   </div>
               </div>
               <div class="row">
                   <div class="col-md-12">
                       <label class="form-text">
                           E-mail
                       </label>
                       <input type="text" class="form-control" placeholder="Введите Ваш e-mail">
                   </div>
               </div>
               <div class="row mb30">
                   <div class="col-md-12">
                       <div class="profile-pagi">
                           <div class="row">
                               <div class="col-md-6 col-md-offset-3">
                                   <label class="chb">
                                       <input type="checkbox">
                                       Сделать платным (поднять в топ)
                                   </label>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>

               <div class="row">
                   <div class="col-md-5">
                       <button type="submit" class="btn btn-info">опубликовать</button>
                   </div>
               </div>
           </div>
       </div>


    </div>







<?php include("footer.php"); ?>